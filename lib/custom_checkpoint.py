import numpy as np
from tensorflow.keras.callbacks import Callback
import os


class BatchModelCheckpoint(Callback):
    def __init__(self, log_folder, log_every):
        super(BatchModelCheckpoint, self).__init__()
        self.log_every = log_every
        self.log_folder = log_folder
        self.current_epoch = 0
        self.last_file = None

    def on_epoch_begin(self, epoch):
        self.current_epoch = epoch

    def on_train_batch_end(self, batch):
        if np.mod(batch+1, self.log_every) == 0:
            if batch + 1 != self.log_every:
                # automatically remove previously stored models in this epoch
                previous_file = path_to_checkpoint(self.log_folder,
                                                   self.epoch_offset + self.current_epoch + 1,
                                                   self.batch_offset + batch + 1 - self.log_every)

                os.remove(previous_file + '.h5')

            # save the current model
            file = path_to_checkpoint(self.log_folder,
                                      self.epoch_offset + self.current_epoch + 1,
                                      self.batch_offset + batch + 1)
            self.last_file = file + '.h5'
            self.model.save(self.last_file)

    def on_epoch_end(self):
        # automatically remove previously stored models in this epoch
        if self.last_file is not None:
            os.remove(self.last_file)

        # save the current model
        file = path_to_checkpoint(self.log_folder,
                                  self.epoch_offset + self.current_epoch + 1, 'end')
        self.model.save(file + '.h5')


def path_to_checkpoint(folder, epoch, step):
    # add correct amount of zeros to epoch number
    zeros_epoch = ''
    for i in range(2):
        if epoch / (10 ** (i + 1)) < 1:
            zeros_epoch = zeros_epoch + '0'

    # construct path
    if isinstance(step, str):  # this marks the end of the epoch
        path = folder + '/epoch-' + zeros_epoch + str(epoch) + '_batch-' + step
    else:
        # add correct amount of zeros to step number
        zeros_step = ''
        for i in range(4):
            if step / (10 ** (i + 1)) < 1:
                zeros_step = zeros_step + '0'
        path = folder + '/epoch-' + zeros_epoch + str(epoch) + '_batch-' + zeros_step + str(step)
    return path


