import numpy as np
import h5py


def generate_from_hdf5(file_path, indices, batch_size):
    data_labels_hdf5 = h5py.File(file_path, 'r')
    patch_size = np.shape(data_labels_hdf5['Data'][0])[0]

    index_batch = 0
    while True:
        # initialize our batches of images and labels
        data_batch = np.zeros([batch_size, patch_size, patch_size, 1])
        labels_batch = np.zeros([batch_size, patch_size, patch_size, 1])
        mask_batch = np.zeros([batch_size, patch_size, patch_size, 1])

        # keep looping until we reach our batch size
        for i in range(batch_size):
            current_mask = np.array(data_labels_hdf5['Mask'][indices[j]]) == 1  # mask is one at metal trace
            current_data = np.array(data_labels_hdf5['Data'][indices[j]])
            current_label = np.array(data_labels_hdf5['Labels'][indices[j]])
            current_data[current_mask] = 0
            data_batch[i] = current_data
            labels_batch[i] = current_label
            mask_batch[i] = (np.array(current_mask == 0).astype(int))  # invert mask

        # yield the batch to the calling function
        index_batch += 1
        yield (data_batch, mask_batch), labels_batch
