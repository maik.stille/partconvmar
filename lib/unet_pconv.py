# Source: https://github.com/MathiasGruber/PConv-Keras/blob/master/libs/pconv_model.py

from keras.layers import *
from keras.optimizers import Adam
import tensorflow as tf
from tensorflow.keras import backend as K
from network_training.pconv_2D import PConv2D


class UNetworkPartialConvolution:
    def __init__(self, patch_size, optimizer, loss, metrics, verbose=2):
        self.input_size_data = (patch_size, patch_size, 1)
        self.input_size_mask = (patch_size, patch_size, 1)
        self.filters = [64, 128, 256, 512]
        self.optimizer = optimizer
        self.loss = loss
        self.metrics = metrics
        self.verbose = verbose
        self.network = self.compile()

    def model(self):
        input_size_data = self.input_size_data
        input_size_mask = self.input_size_mask
        filters = self.filters

        inputs_data = Input(shape=input_size_data, name='input_data')
        inputs_mask = Input(shape=input_size_mask, name='input_mask')

        # ENCODER
        def encoder_layer(img_in, mask_in, filters, kernel_size, bn=True):
            conv, mask = PConv2D(filters, kernel_size, strides=2, padding='same')([img_in, mask_in])
            if bn:
                conv = BatchNormalization(name='EncBN' + str(encoder_layer.counter))(conv, training=True)
            conv = Activation('relu')(conv)
            encoder_layer.counter += 1
            return conv, mask

        encoder_layer.counter = 0

        # 128 x 1 -> 64 x 64
        e_conv1, e_mask1 = encoder_layer(inputs_data, inputs_mask, filters[0], 5, bn=False)
        # 64 x 64 -> 32 x 128
        e_conv2, e_mask2 = encoder_layer(e_conv1, e_mask1, filters[1], 3)
        # 32 x 128 -> 16 x 256
        e_conv3, e_mask3 = encoder_layer(e_conv2, e_mask2, filters[2], 3)
        # 16 x 256 -> 8 x 512
        e_conv4, e_mask4 = encoder_layer(e_conv3, e_mask3, filters[3], 3)
        # 8 x 512 -> 4 x 512
        e_conv5, e_mask5 = encoder_layer(e_conv4, e_mask4, filters[3], 3)
        # 4 x 512 -> 2 x 512
        e_conv6, e_mask6 = encoder_layer(e_conv5, e_mask5, filters[3], 3)

        # DECODER
        def decoder_layer(img_in, mask_in, e_conv, e_mask, filters, kernel_size, bn=True, relu=True):
            up_img = UpSampling2D(size=(2, 2))(img_in)
            up_mask = UpSampling2D(size=(2, 2))(mask_in)
            concat_img = Concatenate(axis=3)([e_conv, up_img])
            concat_mask = Concatenate(axis=3)([e_mask, up_mask])
            conv, mask = PConv2D(filters, kernel_size, padding='same')([concat_img, concat_mask])
            if bn:
                conv = BatchNormalization()(conv)
            if relu:
                conv = LeakyReLU(alpha=0.2)(conv)
            return conv, mask

        # 2 x 512 -> 4 x 512
        d_conv7, d_mask7 = decoder_layer(e_conv6, e_mask6, e_conv5, e_mask5, filters[3], 3)
        # 4 x 512 -> 8 x 512
        d_conv8, d_mask8 = decoder_layer(d_conv7, d_mask7, e_conv4, e_mask4, filters[3], 3)
        # 8 x 512 -> 16 x 256
        d_conv9, d_mask9 = decoder_layer(d_conv8, d_mask8, e_conv3, e_mask3, filters[2], 3)
        # 16 x 256 -> 32 x 128
        d_conv10, d_mask10 = decoder_layer(d_conv9, d_mask9, e_conv2, e_mask2, filters[1], 3)
        # 32 x 128 -> 64 x 64
        d_conv11, d_mask11 = decoder_layer(d_conv10, d_mask10, e_conv1, e_mask1, filters[0], 3)
        # 64 x 64 -> 128 x 1
        d_conv12, d_mask12 = decoder_layer(d_conv11, d_mask11, inputs_data, inputs_mask, 1, 3, bn=False, relu=False)

        network = tf.keras.Model(inputs=[inputs_data, inputs_mask],
                                 outputs=K.concatenate([K.expand_dims(d_conv12, axis=3),
                                                        K.expand_dims(inputs_mask, axis=3)], axis=3))
        return network

    def compile(self):
        network = self.model()
        optimizer = Adam(learning_rate=0.0002)
        network.compile(optimizer=optimizer, loss=self.loss, metrics=[self.metrics])
        return network
