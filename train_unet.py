import numpy as np
from lib.unet_pconv import *
from lib.custom_loss import *
from lib.generator import generate_from_hdf5
from lib.custom_checkpoint import BatchModelCheckpoint
from tensorflow.keras.callbacks import TerminateOnNaN
import h5py


def main():
    np.random.seed(0)
    epochs = 100
    batch_size = 6
    optimizer = 'adam'
    loss = loss_maetv
    metrics = loss_mse_mask
    patch_size = 128

    # find relevant training and validation file
    data_file = 'data/training.hdf5'
    val_file = 'data/validation.hdf5'

    # find number of data in the files
    h5file = h5py.File(data_file, mode="r")
    number_train = h5file['Data'].len()
    h5file = h5py.File(val_file, mode="r")
    number_val = h5file['Data'].len()

    print('Defining training and validation indices...')
    indices_train = np.arange(number_train)
    np.random.shuffle(indices_train)
    indices_val = np.arange(number_val)
    np.random.shuffle(indices_val)

    # create log directory
    log_dir = 'data/log/'

    # calculate steps of one epoch
    steps_train = number_train // batch_size
    steps_val = number_val // batch_size

    print('Defining network... ')
    unet = UNetworkPartialConvolution(patch_size, optimizer, loss, metrics)

    print('Starting to train...')
    train_network(
        unet.network,
        [data_file, val_file],
        epochs,
        batch_size,
        steps_train,
        indices_train,
        steps_val,
        indices_val,
        log_dir)


def train_network(network, files, epochs, batch_size, steps_train, indices_train, steps_val, indices_val,
                  log_dir, log_every=3000):
    terminate = TerminateOnNaN()
    checkpoint = BatchModelCheckpoint(log_dir, log_every)

    generator = generate_from_hdf5(files[0], indices_train, batch_size)
    generator_val = generate_from_hdf5(files[1], indices_val, batch_size)

    network.fit(
        x=generator,
        steps_per_epoch=steps_train,
        validation_data=generator_val,
        validation_steps=steps_val,
        epochs=epochs,
        callbacks=[checkpoint, terminate])

    return network


if __name__ == '__main__':
    main()
