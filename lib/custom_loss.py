import tensorflow as tf
from tensorflow.keras import backend as K


def MAE(y_true, y_pred):
    if K.ndim(y_true) == 4:
        return K.mean(K.abs(y_pred - y_true), axis=[1, 2, 3])
    else:
        raise NotImplementedError("This output shape is not implemented for this network (got + "
                                  + str(K.ndim(y_true)) + " instead of 4")


def l1(y_true, y_pred):
    return K.sum(K.abs(y_pred - y_true), axis=[1, 2, 3])


def l2(y_true, y_pred):
    return K.sum(K.abs(y_pred - y_true), axis=[1, 2, 3])


def loss_mae_mask(y_true, y_pred):
    mask = y_pred[:, :, :, 1]
    mask = K.expand_dims(mask, axis=3)
    y_pred = y_pred[:, :, :, 0]
    y_pred = K.expand_dims(y_pred, axis=3)
    l1_mask = l1((1 - mask) * y_true, (1 - mask) * y_pred)
    number_elements_mask = l1(mask, K.ones_like(mask))
    number_elements_mask = tf.where(number_elements_mask == 0., 1., number_elements_mask)
    return l1_mask / number_elements_mask


def loss_mse_mask(y_true, y_pred):
    mask = y_pred[:, :, :, 1]
    mask = K.expand_dims(mask, axis=3)
    y_pred = y_pred[:, :, :, 0]
    y_pred = K.expand_dims(y_pred, axis=3)
    l2_mask = l2((1 - mask) * y_true, (1 - mask) * y_pred)
    number_elements_mask = l2(mask, K.ones_like(mask))
    number_elements_mask = tf.where(number_elements_mask == 0., 1., number_elements_mask)
    return l2_mask / number_elements_mask


def loss_maetv(y_true, y_pred):
    l_mae = loss_mae_mask(y_true, y_pred)

    mask = y_pred[:, :, :, 1]
    mask = K.expand_dims(mask, axis=3)
    y_pred = y_pred[:, :, :, 0]
    y_pred = K.expand_dims(y_pred, axis=3)
    y_comp = mask * y_true + (1 - mask) * y_pred
    l_tv = loss_tv(mask, y_comp)
    return l_mae + l_tv


# Source: https://github.com/MathiasGruber/PConv-Keras/blob/master/libs/pconv_model.py
def loss_tv(mask, y_comp):
    # Create dilated hole region using a 3x3 kernel of all 1s.
    kernel = tf.ones([3, 3, 1], dtype=tf.float32)
    dilated_mask = tf.nn.dilation2d(1-mask, kernel, strides=[1, 1, 1, 1], padding='SAME',
                                    data_format='NHWC', dilations=[1, 1, 1, 1])

    # Cast values to be [0., 1.], and compute dilated hole region of y_comp
    dilated_mask_bool = K.cast(K.greater(dilated_mask, 1), tf.float32)
    P = dilated_mask_bool * y_comp

    # Calculate total variation loss
    a = MAE(P[:, 1:, :, :], P[:, :-1, :, :])
    b = MAE(P[:, :, 1:, :], P[:, :, :-1, :])
    loss = a + b
    return loss

